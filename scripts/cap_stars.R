array1 <- array(1:48, dim =c(4,4,3))
array1

library(stars)
library(wk)
library(dplyr)
setwd("/home/jf/pCloudDrive/libroRSIG2022")
s_l8 <- read_stars("recursos/landsat/L8_20210429.tif")
s_l8

class(s_l8)
length(s_l8)

st_dimensions(s_l8)
plot(s_l8)

bands = c("Azul", "Verde", "Rojo", "NIR", "SWIR1", "SWIR2")
s_l8 = st_set_dimensions(s_l8, "band", values = bands)
plot(s_l8)

l8.spl <- split(s_l8, "band")
l8.spl
class(l8.spl$Azul)

merge(l8.spl)

b_s2 <- s_l8 |> slice(band, 2)
b_s2

library(cubelyr)
b_s3 <- s_l8 |> filter(band == "Rojo")

## unidades de x en coordenadas UTM 
b35_l8  <- s_l8 |> filter(x > 632457 , x < 649624, band %in% c("Rojo","SWIR1")) 
plot(b35_l8)

## x del pixel 1 al 500
plot(s_l8[,1:500,, 3:4]) 


pol <- st_polygon(list(rbind(c(617231.2395, 2276018.9324),
                     c(617231.2395, 2295833.8943),
                     c(638926.45327, 2295833.8943),
                     c(638926.4532, 2276018.9324),
                     c(617231.2395, 2276018.9324)))) |>
                      st_sfc() 
st_crs(pol) = 32613 

recorte <- st_crop(s_l8, pol)
plot(recorte)

plot(s_l8[pol][, , , 6]) 

s_l8.a = s_l8[,100:200,100:200,]
s_l8.b = s_l8[,200:400,200:300,]
plot(st_mosaic(s_l8.a, s_l8.b))

pnt <- st_sample(st_as_sfc(st_bbox(s_l8)), 10)
pnt_sample <- st_extract(s_l8, pnt)
pnt_sample

pnt_sample |> st_as_sf()

aggregate(s_l8, by = pol, FUN = mean) |> st_as_sf()

s500 <- st_as_stars(st_bbox(s_l8), dx = 500)
s_l8_500 <- st_warp(s_l8,s500, use_gdal = TRUE, fun = mean)
s_l8_500
plot(s_l8_500, rgb = c(4, 3, 2))

rs <- (s_l8 * 0.0000275) - 0.2
plot(rs)

red = rs[,,,3, drop = TRUE]
nir = rs[,,,4, drop = TRUE]
ndvi.a = (nir - red) / (nir + red)
names(ndvi.a) = "NDVI"

plot(ndvi.a, col = rev(hcl.colors(10, "Geyser")))

### escalamiento de la refeclectancia
st_apply(X = s_l8, MARGIN = 1:2, FUN = function(x)((x * 0.0000275) - 0.2))

f_ndvi = function(red,nir) (nir-red)/(nir+red) 
st_apply(rs[,,,3:4], MARGIN = 1:2,f_ndvi, CLUSTER = 2)

st_apply(s_l8, MARGIN = 1:2, mean)

st_apply(s_l8, MARGIN = 3, mean)[[1]]

rasterio <- list(nXOff = 10,nYOff = 5,nXSize = 100,nYSize = 100, bands = c(1,3,4))

s0 <- read_stars("recursos/landsat/L8_20210429.tif", RasterIO = rasterio)
dim(s0)
dim(s_l8)

s_m <- read_stars("recursos/landsat/L8_20210429.tif", proxy = TRUE)
s_m

object.size(s_m)
object.size(s_l8)

ndvi_proxy <- st_apply(s_m[,,,3:4], MARGIN = 1:2, 
                       FUN = function(x)((x * 0.0000275) - 0.2)) |>
st_apply(MARGIN = 1:2, f_ndvi, CLUSTER = 2)


temp <- read_stars("recursos/modis/LST_Day_1km.tif", proxy = FALSE)
plot(temp[,,,1:12])

temp_celcius  <-  function(x){(x * 0.02) - 273.15}
temp_c <- st_apply(temp, 1:2 , FUN = temp_celcius)
fechas <- read.csv("recursos/modis/fechas.csv")[,1] |> as.Date()
temp_c <- st_set_dimensions(temp_c, names = c("time", "x", "y"))
temp_c <- st_set_dimensions(temp_c, "time", values = fechas)
temp_c

by_t = "1 months"
temp_m <- aggregate(temp_c, by = by_t, FUN = mean, na.rm = TRUE)
plot(temp_m, col = hcl.colors(25, "YlOrRd"))

admin <- st_read("recursos/14mun.gpkg")
admin_mt <- aggregate(temp_m, admin, FUN = mean)
admin_mt
