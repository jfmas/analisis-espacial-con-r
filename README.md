# Análisis espacial con R: Usa R como un sistema de información geográfica

## Recursos para los ejemplos  y casos de estudio del libro "Análisis Espacial con R"

Los scripts del libro se encuentran en la carpeta _scripts_, mientras los archivos (imágenes, tablas, mapas, etc.) se comprimieron en un archivo zip para cada capítulo. 
